:program:`vcsgc-lammps` — A Monte Carlo module for lammps
*********************************************************

The :program:`vcsgc-lammps` package is an extension for the popular molecular
dynamics (MD) code `LAMMPS <http://lammps.sandia.gov/>`_ [Pli95]_, which
enables efficient Monte Carlo (MC) simulations in the semi-grand-canonical
(SGC) and variance constrained SGC ensembles [SadErhStu12]_, [SadErh12]_.  It
directly supports `eam` pair styles. Other pair styles can be run but the
execution speed will be *much* slower since the simulation will require
reevaluation of the total energy at each step.

If you publish results obtained using this extension please cite
[SadErhStu12]_ and when employing the serial VC-SGC version also [SadErh12]_.

:program:`vcsgc-lammps` is hosted on `gitlab
<https://gitlab.com/materials-modeling/vcsgc-lammps>`_. Bugs and feature
requests can be submitted via the
`gitlab issue tracker
<https://gitlab.com/materials-modeling/vcsgc-lammps/issues>`_ or by email via
vcsgc-lammps@materialsmodeling.org.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   installation
   usage
   bibliography
