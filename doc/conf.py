#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import re
import sphinx_rtd_theme

extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    'sphinx_sitemap']

site_url = 'https://vcsgc-lammps.materialsmodeling.org/'
version = ''
release = ''
copyright = '2012-2018'
project = 'vcsgc-lammps'
author = 'Alexander Stukowski, Paul Erhart'

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = 'sphinx'
todo_include_todos = True

site_url = 'https://vcsgc-lammps.materialsmodeling.org/'
html_logo = "_static/logo.png"
html_favicon = "_static/logo.ico"
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_static_path = ['_static']
html_theme_options = {'display_version': False}
html_context = {
    'software':
        [('atomicrex',
          'https://atomicrex.org/',
          'interatomic potential construction'),
         ('dynasor',
          'https://dynasor.materialsmodeling.org/',
          'dynamical structure factors from MD'),
         ('hiPhive',
          'https://hiphive.materialsmodeling.org/',
          'anharmonic force constant potentials'),
         ('icet',
          'https://icet.materialsmodeling.org/',
          'cluster expansions'),
         ('libvdwxc',
          'https://libvdwxc.org/',
          'library for van-der-Waals functionals'),
         ('storq',
          'https://storq.materialsmodeling.org/',
          'high-throughput submission system'),
         ('vcsgc-lammps',
          'https://vcsgc-lammps.materialsmodeling.org/',
          'Monte Carlo simulations with lammps'),
         ]}
htmlhelp_basename = 'vcsgc-lammps-help'

# -- Options for LaTeX output ---------------------------------------------
_PREAMBLE = r"""
\usepackage{amsmath,amssymb}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\argmin}{\arg\!\min}
"""

latex_elements = {
    'preamble': _PREAMBLE,
}
latex_documents = [
    (master_doc,
     'vcsgc-lammps.tex',
     'vcsgc-lammps Documentation',
     'Alexander Stukowski, Paul Erhart',
     'manual'),
]

# -- Options for manual page output ---------------------------------------
man_pages = [
    (master_doc,
     'vcsgc-lammps',
     'vcsgc-lammps Documentation',
     [author], 1)
]

# -- Options for Texinfo output -------------------------------------------
texinfo_documents = [
    (master_doc,
     'vcsgc-lammps',
     'vcsgc-lammps Documentation',
     author,
     'vcsgc-lammps',
     'A Monte Carlo module for lammps',
     'Miscellaneous'),
]
