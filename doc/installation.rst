.. _installation:
.. index:: Installation

Installation
************

Note that :program:`vcsgc-lammps` first requires you to compile
:program:`lammps` from source. Please consult the `lammps homepage
<http://lammps.sandia.gov/>`_ for instructions.

Preparations
============

You can retrieve the most recent version of :program:`vcsgc-lammps` by
executing the following commands in the :program:`lammps` ``src/``
directory::

  wget https://vcsgc-lammps.materialsmodeling.org/vcsgc-lammps.tgz
  tar xfz vcsgc-lammps.tgz

This will extract the source code of the module to the ``USER-VCSGC/`` subdirectory.

The next steps depend on whether you use the `make
<https://lammps.sandia.gov/doc/Build_make.html>`_ or `cmake
<https://lammps.sandia.gov/doc/Build_cmake.html>`_ routes for
compiling :program:`lammps`.

``Make``
========

Activate the module by running::

  make yes-user-vcsgc

:program:`vcsgc-lammps` is intended to be used with many-body
potentials such as the embedded atom method (EAM). It is therefore
necessary to activate the ``MANYBODY`` extension by running::

  make yes-manybody

Finally, compile :program:`lammps` as usual with the command ``make
<machine-name>``.

``CMake``
=========

The general approach when using ``cmake`` is described `here
<https://lammps.sandia.gov/doc/Build_cmake.html>`_ with additional
information provided in a more extended `tutorial
<https://lammps.sandia.gov/doc/Howto_cmake.html>`_. The
:program:`vcsgc-lammps` is activated via the ``PKG_USER-VCSGC``
option. To make this option available the ``cmake/CMakeLists.txt``
file needs to be patched. This is achieved by running (assuming that
the command is run in the :program:`lammps` root directory)::

  patch cmake/CMakeLists.txt src/USER-VCSGC/CMakeLists.txt.patch

Then if you follow the `standard instructions
<https://lammps.sandia.gov/doc/Build_cmake.html>`_, you would run,
starting in the :program:`lammps` root directory,::

  mkdir build; cd build    # create and use a build directory 
  cmake ../cmake           # configuration reading CMake scripts from ../cmake

Before initiating the actual compilation you could now activate
:program:`vcsgc-lammps` via::

  cmake -D PKG_USER-VCSGC=on .

Finally, compile :program:`lammps` as usual with the commands ``make``
or ``cmake --build .``.

Testing your compilation
========================

After the compilation has completed, you can test the installation
by running the included example :program:`lammps` script::

  cd src/USER-VCSGC/examples/
  my_lmp -in example_input_sgcmc.in

Here, ``my_lmp`` should be replaced with the path to your :program:`lammps`
executable.

**Note to users of the 'eam/cd' pair style:**
If you plan to use the MC routine with the concentration-dependent EAM model
(`pair_style eam/cd <http://lammps.sandia.gov/doc/pair_eam.html>`_), you also have to install this pair
style by running ``make yes-user-misc``. Then the preprocessor definition
``CDEAM_MC_SUPPORT`` at the top of the source file ``fix_semigrandcanonical_mc.h`` has to be set to 1. 
