.. _bibliography:
.. index:: Bibliography

Bibliography
************

.. [Pli95]
       | S. Plimpton
       | *Fast Parallel Algorithms for Short-Range Molecular Dynamics*
       | J. Comput. Phys. **117**, 1 (1995)
       | `doi:10.1006/jcph.1995.1039 <http://dx.doi.org/10.1103/PhysRevB.85.184203>`_

.. [SadErhStu12]
       | B. Sadigh, P. Erhart, A. Stukowski, A. Caro, E. Martinez, and L. Zepeda-Ruiz
       | *Scalable parallel Monte Carlo algorithm for atomistic simulations of precipitation in alloys*
       | Phys. Rev. B **85**, 184203 (2012)
       | `doi:10.1103/PhysRevB.85.184203 <http://dx.doi.org/10.1103/PhysRevB.85.184203>`_

.. [SadErh12]
       | B. Sadigh and P. Erhart
       | *Calculations of excess free energies of precipitates via direct thermodynamic integration across phase boundaries*
       | Phys. Rev. B **86**, 134204 (2012)
       | `doi:10.1103/PhysRevB.86.134204 <http://dx.doi.org/10.1103/PhysRevB.86.134204>`_

.. [StuSadErh09]
       | A. Stukowski, B. Sadigh, P. Erhart, and A. Caro
       | *Efficient implementation of the concentration-dependent embedded atom method for molecular dynamics and Monte-Carlo simulations*
       | Modelling and Simulation in Materials Science and Engineering **17**, 075005 (2009)
       | `doi:10.1088/0965-0393/17/7/075005 <http://dx.doi.org/10.1088/0965-0393/17/7/075005>`_
